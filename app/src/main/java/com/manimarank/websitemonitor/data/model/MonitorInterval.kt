package com.manimarank.websitemonitor.data.model

data class MonitorInterval (
    val name: String,
    val duration: Long
)